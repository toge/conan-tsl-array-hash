import os

from conans import ConanFile, tools

class TslArrayHashConan(ConanFile):
    name = "tsl-array-hash"
    version = "0.7.1"
    license = "MIT"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-tsl-array-hash/"
    homepage = "https://github.com/Tessil/array-hash"
    description = "C++ implementation of a fast and memory efficient hash map and hash set specialized for strings."
    topics = ("hash-map", "header-only",)
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get("https://github.com/Tessil/array-hash/archive/v{}.zip".format(self.version))
        os.rename("array-hash-{}".format(self.version), "array-hash")
        # self.run("git clone ...") or
        # tools.download("url", "file.zip")
        # tools.unzip("file.zip" )

    def package(self):
        self.copy("*.h", "include/tsl", src="array-hash/include/tsl")
